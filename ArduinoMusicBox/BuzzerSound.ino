#include "pitches.h"

int melodia[] = {
  NOTE_C4, NOTE_F4, NOTE_F4, NOTE_G4, NOTE_A4, NOTE_F4, NOTE_C5,
  NOTE_AS4, NOTE_A4, NOTE_A4, NOTE_AS4, NOTE_C5, NOTE_AS4, NOTE_A4,
  NOTE_AS4, NOTE_C5, NOTE_G4, NOTE_F4, NOTE_G4, NOTE_A4, NOTE_G4
};

int durataNote[] = {
  8, 8, 16, 16, 8, 8, 6, 16, 6, 16, 8, 16, 16, 16, 16, 8, 16, 16, 16, 16, 6
};

int numeroNote = 21;


// Pin a cui è collegato il ponte con il fotoresistore

const int sensorPin = 0;
const int buzzerPin = 3;

// Inizializzo le variabili
int lightCal;
int lightVal;


void setup()
{
  // We'll set up the LED pin to be an output.
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(buzzerPin, OUTPUT);
  lightCal = analogRead(sensorPin);
  //memorizzo il valore di luminosità di riferimento
  Serial.begin(9600); // open the serial port at 9600 bps:
}


void loop()
{
  //Take a reading using analogRead() on sensor pin and store it in lightVal
  lightVal = analogRead(sensorPin);
  //Serial.println(lightVal);

  if (lightVal > lightCal - 50)
  {
    digitalWrite(13, HIGH);
      for(int i = 0; i < numeroNote; i++){
    int durata = 1500 / durataNote[i];
    tone(buzzerPin, melodia[i], durata);
    delay(durata * 1.3);
  }
  }


  {
    digitalWrite(13, LOW);
    noTone(buzzerPin);
  }

}
